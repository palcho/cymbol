%{
#   include "y.tab.h"
#   define print(...) fprintf(stderr, __VA_ARGS__)
%}

%%

int                       { print("l: a type int\n"); return TYPEINT; }
return                    { print("l: a return\n"); return RETURN; }
[a-zA-Z][a-zA-Z0-9]*      { print("l: an identifier %s\n", yytext); yylval.id = yytext; return ID; }
[0-9]+                    { print("l: a number %d\n", atoi(yytext)); yylval.num = atoi(yytext); return NUMBER; }
"/*"([^*]|[*][^/])*?"*/"  { print("l: a block comment\n"); }
"//".*?\n                 { print("l: a line comment\n"); }
[ \t\r\n]                 { ; }
[(),;{}=*/+-]             { print("l: the special character '%c'\n", *yytext); return *yytext; }
.                         { print("l: ERROR unexpected character %c\n", *yytext); }

%%

int yywrap (void) {
    return 1;
}

