%{
/* C declarations used in actions */
#   include <stdio.h> 
#   include <stdlib.h>
#   include <string.h>
#   include <inttypes.h>
#   include <math.h>
#   define print(...) fprintf(stderr, __VA_ARGS__)

    extern int yylex (void);
    void yyerror (char *str);
%}
/* yacc definitions */
%union { int num; char *id; } /* types that may be returned */
%start file              /* start token */
%token TYPEINT RETURN
%token /*<num>*/ NUMBER       /* type of terminals (on the right side of the grammar) */
%token /*<id>*/ ID
/*%type <num> file expr term assignment call*/ /* type of non-terminals on the left side of the grammar */
/*%type <id> definition param */
%left '+' '-'             /* orders of precedence */
%left '*' '/'
%left UMINUS UPLUS        /* unary minus */

%%

/* description of expected inputs :  corresponding actions (in C) */
file       : funcDecl           { print("y: file -> funcDecl\n"); } 
           | file funcDecl      { print("y: file -> file funcDecl\n"); }
           | varDecl            { print("y: file -> varDecl\n"); }
           | file varDecl       { print("y: file -> file varDecl\n"); }
           ;

varDecl    : type ID '=' expr ';'    { print("y: varDecl -> type ID = expr;\n"); }
           | type ID ';'             { print("y: varDecl -> type ID;\n"); }
           ;

type       : TYPEINT            { print("y: type -> TYPEINT\n"); }
           ;

funcDecl   : type ID '(' paramTypeList ')' block   { print("y: funcDecl -> type ID ( paramTypeList ) block\n"); }
           | type ID '(' ')' block                 { print("y: funcDecl -> type ID () block\n"); }
           ;

paramTypeList  : paramType                       { print("y: paramTypeList -> paramType\n"); } 
               | paramTypeList ',' paramType     { print("y: paramTypeList -> paramType, paramType\n"); }
               ;

paramType  : type ID            { print("y: paramType -> type ID\n"); }
           ;

block      : '{' statBlock '}'  { print("y: block -> { statBlock }\n"); }
           | '{' '}'            { print("y: block -> { }\n"); }
           ;

assignStat : ID '=' expr ';'    { print("y: assignStat -> ID = expr;\n"); }
           ;

returnStat : RETURN expr ';'    { print("y: returnStat -> RETURN expr;\n"); }
           ;

exprStat   : expr ';'           { print("y: exprStat -> expr;\n"); }
           ;

exprList   : expr               { print("y: exprList -> expr\n"); }
           | exprList ',' expr  { print("y: exprList -> exprList, expr\n"); }
           ;

statBlock  :  stat               { print("y: statBlock -> stat\n"); }
           |  statBlock stat     { print("y: statBlock -> statBlock stat\n"); }
           ;

stat       : varDecl            { print("y: stat -> varDecl\n"); }
           | returnStat         { print("y: stat -> returnStat\n"); }
           | assignStat         { print("y: stat -> assignStat\n"); }
           | exprStat           { print("y: stat -> exprStat\n"); }
           ;

funcCall   : ID '(' exprList ')'   { print("y: funcCall -> ID ( exprList )\n"); }
           | ID '(' ')'            { print("y: funcCall -> ID ()\n"); }
           ;

expr       : expr '+' expr          { print("y: expr -> expr + expr\n"); }
           | expr '-' expr          { print("y: expr -> expr - expr\n"); }
           | expr '*' expr          { print("y: expr -> expr * expr\n"); }
           | expr '/' expr          { print("y: expr -> expr / expr\n"); }
           | '-' expr %prec UMINUS  { print("y: expr -> - expr\n"); }
           | '+' expr %prec UPLUS   { print("y: expr -> + expr\n"); }
           | '(' expr ')'           { print("y: expr -> ( expr )\n"); }
           | funcCall               { print("y: expr -> funcCall\n"); }
           | term                   { print("y: expr -> term\n"); }
           ;

term       : NUMBER                 { print("y: term -> NUMBER\n"); }
           | ID                     { print("y: term -> ID\n"); }
           ;

%%

/* C code */
int main (void) {
    return yyparse();
}

void yyerror (char *str) {
    fprintf(stderr, "%s\n", str);
}

