%{
#   include "ast.h"
#   include "y.tab.h"
#   define print(...) fprintf(stderr, __VA_ARGS__)
%}

%%

int                       { return TYPEINT; }
return                    { return RETURN; }
[a-zA-Z][a-zA-Z0-9]*      { yylval.id = yytext; return ID; }
[0-9]+                    { yylval.num = atoi(yytext); return NUMBER; }
"/*"([^*]|[*][^/])*?"*/"  { ; }
"//".*?\n                 { ; }
[ \t\r\n]                 { ; }
[(),;{}=*/+-]             { return *yytext; }
.                         { print("l: ERROR unexpected character %c\n", *yytext); }

%%

int yywrap (void) {
    return 1;
}

