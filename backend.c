#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <alloca.h>
#include "ast.h"

typedef struct ExprReturn_ ExprReturn;
struct ExprReturn_ {
	union {
		int ir_number;
		int cte_value;
	};
    bool is_cte;
};

void meet_file (AST *root);
void meet_global_varDecl (AST *ast);
void meet_funcDecl (AST *ast);
void meet_block (AST *block, AST *param);
void meet_stat (AST *stat);
void meet_assignStat (AST *assign);
void meet_returnStat (AST *assign);
void meet_varDecl (AST *ast);
ExprReturn meet_expr (AST *expr);
ExprReturn meet_add (AST *expr);
ExprReturn meet_sub (AST *expr);
ExprReturn meet_mul (AST *expr);
ExprReturn meet_div (AST *expr);
ExprReturn meet_id (AST *ast);
ExprReturn meet_number (AST *ast);
ExprReturn meet_uminus (AST *ast);
ExprReturn meet_funcCall (AST *ast);

int current_scope = 0;
int next_scope = 1;
int next_ir_number;
int obsolete_ir_for_globals;

void meet_block (AST *block, AST *params) {
    current_scope = next_scope++;
    next_ir_number = 0;
    obsolete_ir_for_globals = 0;

    if (params != NULL) {
        //AST *id = ptr->ast->paramType.id;
        for (List *ptr = params->list.first; ptr != NULL; ptr = ptr->next) {
            ptr->ast->paramType.id->id.last_scope = current_scope;
            ptr->ast->paramType.id->id.ir_number = next_ir_number++;
            ptr->ast->paramType.id->id.is_cte = false;
        }
    }
    next_ir_number++;

    for (List *ptr = block->list.first; ptr != NULL; ptr = ptr->next) {
        meet_stat(ptr->ast);
    } 
    current_scope = 0; // global scope
}

void meet_funcDecl (AST *ast) {
    AST *params = ast->decl.funcDecl.paramTypeList;
    printf("define i32 @%s(", ast->decl.funcDecl.id->id.id_str);
    if (params != NULL) {
        printf("i32");
        for (int i = 1; i < params->list.num_items; i++) {
            printf(", i32");
        }
    }
    printf(") {\n");
    if (ast->decl.funcDecl.block != NULL) {
        meet_block(ast->decl.funcDecl.block, params);
    }
    printf("}\n\n");
}



void meet_global_varDecl (AST *ast) {
    AST *id = ast->decl.varDecl.id;
    printf("@%s = global i32 ", id->id.id_str);
    id->id.is_global = true;
    id->id.is_cte = false;
    if (ast->decl.varDecl.expr != NULL) {
        ExprReturn ret = meet_expr(ast->decl.varDecl.expr);
        printf("%d", ret.cte_value);
    } else {
        printf("0");
    }
    printf("\n\n");
    // @if ret expr is not constant COMPLAIN!
}

void meet_stat (AST *stat) {
    switch (stat->stat.stat_type) {
        case 'd': meet_varDecl(stat); break;
        case '=': meet_assignStat(stat); break;
        case 'r': meet_returnStat(stat); break;
        case 'e': meet_expr(stat->stat.exprStat.expr); break;
        default: fprintf(stderr, "UNKNOWN STATEMENT TYPE %c\n", stat->stat.stat_type); break;
    }
}

void meet_varDecl (AST *ast) {
    AST *id = ast->stat.varDecl.id;

    // @implement what these booleans mean
    if (ast->stat.varDecl.expr != NULL) {
        ExprReturn expr = meet_expr(ast->stat.varDecl.expr);
        if (expr.is_cte) {
            id->id.is_cte = true;
            id->id.cte_value = expr.cte_value;
        } else {
            id->id.is_cte = false;
            id->id.ir_number = expr.ir_number;
        }
    } else {
        id->id.cte_value = 0; // return 0 from calling an id unitialized
        id->id.is_cte = true;
    }
    if (id->id.is_global) {
        id->id.is_global_in_this_scope = false;
    }
    id->id.last_scope = current_scope;
}


void meet_returnStat (AST *ast) {
    ExprReturn expr = meet_expr(ast->stat.returnStat.expr);
    if (expr.is_cte) {
        printf("  ret i32 %d\n", expr.cte_value);
    } else {
        printf("  ret i32 %%%d\n", expr.ir_number);
    }
}

void meet_assignStat (AST *assign) {
    ExprReturn expr = meet_expr(assign->stat.assignStat.expr);
    AST *id = assign->stat.assignStat.id;

    if (id->id.is_global && id->id.last_scope != current_scope) {
        id->id.is_global_in_this_scope = true;
        id->id.last_scope = current_scope;
    }
    if (id->id.is_global_in_this_scope) {
        if (expr.is_cte) {
            id->id.ir_number = next_ir_number++;
            printf("  store i32 %d, i32* @%s\n", expr.cte_value, id->id.id_str);
            printf("  %%%d = load i32, i32* @%s\n", id->id.ir_number, id->id.id_str);
        } else {
            printf("  store i32 %%%d, i32* @%s\n", expr.ir_number, id->id.id_str);
            id->id.ir_number = expr.ir_number;
        }
    } else {
        if (expr.is_cte) {
            id->id.is_cte = true;
            id->id.cte_value = expr.cte_value;
        } else {
            id->id.is_cte = false;
            id->id.ir_number = expr.ir_number;
        }
    }
}

ExprReturn meet_expr (AST *expr) {
    ExprReturn ret = { -1, false };
    switch (expr->expr.expr_type) {
        case '+': ret = meet_add(expr); break;
        case '-': ret = meet_sub(expr); break;
        case '*': ret = meet_mul(expr); break;
        case '/': ret = meet_div(expr); break;
        case 'm': ret = meet_uminus(expr); break;
        case 'n': ret = meet_number(expr); break;
        case 'i': ret = meet_id(expr->expr.id.id); break;
        case 'c': ret = meet_funcCall(expr); break;
        default: fprintf(stderr, "UNKNOWN EXPRESSION TYPE %c\n", expr->expr.expr_type); break;
    }
    return ret;
}

ExprReturn meet_funcCall (AST *ast) {
    ExprReturn ret;
    AST *arg_list = ast->expr.funcCall.exprList;
    ExprReturn *arg_expr = NULL;

    if (arg_list != NULL) {
        arg_expr = alloca(arg_list->list.num_items * sizeof(ExprReturn));
        int i = 0;
        for (List *ptr = arg_list->list.first; ptr != NULL; ptr = ptr->next) {
            arg_expr[i++] = meet_expr(ptr->ast);
        }
    }
    ret = (ExprReturn){ next_ir_number++, false };
    printf("  %%%d = call i32 @%s(", ret.ir_number, ast->expr.funcCall.id->id.id_str);

    if (arg_expr != NULL) {
        if (arg_expr[0].is_cte) {
            printf("i32 %d", arg_expr[0].cte_value);
        } else {
            printf("i32 %%%d", arg_expr[0].ir_number);
        }
        for (int i = 1; i < arg_list->list.num_items; i++) {
            if (arg_expr[i].is_cte) {
                printf(", i32 %d", arg_expr[i].cte_value);
            } else {
                printf(", i32 %%%d", arg_expr[i].ir_number);
            }
        }
    }
    printf(")\n");
    obsolete_ir_for_globals = next_ir_number - 1;
    return ret;
} 

ExprReturn meet_id (AST *ast) {
    ExprReturn ret;
    if (ast->id.is_cte) {
        ret = (ExprReturn){ ast->id.cte_value, true };
    } else {
        if (ast->id.is_global && ast->id.last_scope != current_scope) {
            ast->id.last_scope = current_scope;
            ast->id.ir_number = next_ir_number++;
            ast->id.is_global_in_this_scope = true;
            printf("  %%%d = load i32, i32* @%s\n", ast->id.ir_number, ast->id.id_str);
        } else if (ast->id.last_scope == current_scope && ast->id.is_global_in_this_scope && ast->id.ir_number < obsolete_ir_for_globals) {
            ast->id.ir_number = next_ir_number++;
            printf("  %%%d = load i32, i32* @%s\n", ast->id.ir_number, ast->id.id_str);
        } 
        ret = (ExprReturn){ ast->id.ir_number, false };
    }
    return ret;
}

ExprReturn meet_number (AST *ast) {
    ExprReturn ret;
    ret = (ExprReturn){ ast->expr.number.value, true };
    return ret;
}

ExprReturn meet_uminus (AST *ast) {
    ExprReturn expr, ret;
    expr = meet_expr(ast->expr.uminus.expr);
    if (expr.is_cte) {
        ret = (ExprReturn){ -expr.cte_value, true };
    } else {
        ret = (ExprReturn){ next_ir_number++, false };
        printf("  %%%d = sub nsw i32 0, %%%d\n", ret.ir_number, expr.ir_number);
    }
    return ret;
}

void print_operands (ExprReturn *left, ExprReturn *right) {
    if (left->is_cte) {
        printf(" %d,", left->cte_value);
    } else {
        printf(" %%%d,", left->ir_number);
    }
    if (right->is_cte) {
        printf(" %d\n", right->cte_value);
    } else {
        printf(" %%%d\n", right->ir_number);
    }
}

ExprReturn meet_add (AST *ast) {
    ExprReturn left, right, ret;
    left  = meet_expr(ast->expr.binExpr.leftExpr);
    right = meet_expr(ast->expr.binExpr.rightExpr);

    if (left.is_cte && right.is_cte) {
        ret = (ExprReturn){ left.cte_value + right.cte_value, true };
    } else {
        ret = (ExprReturn){ next_ir_number++, false };
        printf("  %%%d = add nsw i32", ret.ir_number);
        print_operands(&left, &right);
    }
    return ret;
}

ExprReturn meet_sub (AST *ast) {
    ExprReturn left, right, ret;
    left  = meet_expr(ast->expr.binExpr.leftExpr);
    right = meet_expr(ast->expr.binExpr.rightExpr);

    if (left.is_cte && right.is_cte) {
        ret = (ExprReturn){ left.cte_value - right.cte_value, true };
    } else {
        ret = (ExprReturn){ next_ir_number++, false };
        printf("  %%%d = sub nsw i32", ret.ir_number);
        print_operands(&left, &right);
    }
    return ret;
}

ExprReturn meet_mul (AST *ast) {
    ExprReturn left, right, ret;
    left  = meet_expr(ast->expr.binExpr.leftExpr);
    right = meet_expr(ast->expr.binExpr.rightExpr);

    if (left.is_cte && right.is_cte) {
        ret = (ExprReturn){ left.cte_value * right.cte_value, true };
    } else {
        ret = (ExprReturn){ next_ir_number++, false };
        printf("  %%%d = mul nsw i32", ret.ir_number);
        print_operands(&left, &right);
    }
    return ret;
}

ExprReturn meet_div (AST *ast) {
    ExprReturn left, right, ret;
    left  = meet_expr(ast->expr.binExpr.leftExpr);
    right = meet_expr(ast->expr.binExpr.rightExpr);

    if (left.is_cte && right.is_cte) {
        ret = (ExprReturn){ left.cte_value / right.cte_value, true };
    } else {
        ret = (ExprReturn){ next_ir_number++, false };
        printf("  %%%d = sdiv i32", ret.ir_number);
        print_operands(&left, &right);
    }
    return ret;
}



void meet_file (AST *root) {
    printf("; file has %d declarations\n\n", root->list.num_items);

    for (List *ptr = root->list.first; ptr != NULL; ptr = ptr->next) {
        switch (ptr->ast->decl.decl_type) {
            case 'f': meet_funcDecl(ptr->ast); break;
            case 'd': meet_global_varDecl(ptr->ast); break;
            default: fprintf(stderr, "UNKNOWN DECLARATION TYPE %c\n", ptr->ast->decl.decl_type); break;
        }
    }
}
