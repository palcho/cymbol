; ModuleID = 'test.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

@moi = global i32 2, align 4

; Function Attrs: nounwind uwtable
define i32 @arroz(i32 %pachycephalosaurus, i32 %hypslophodon) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %brachiosaurus = alloca i32, align 4
  %velociraptor = alloca i32, align 4
  store i32 %pachycephalosaurus, i32* %1, align 4
  store i32 %hypslophodon, i32* %2, align 4
  %3 = load i32, i32* %1, align 4
  %4 = sdiv i32 %3, 7
  store i32 %4, i32* %brachiosaurus, align 4
  %5 = load i32, i32* %brachiosaurus, align 4
  %6 = sub nsw i32 0, %5
  %7 = load i32, i32* @moi, align 4
  %8 = mul nsw i32 %6, %7
  store i32 %8, i32* %velociraptor, align 4
  %9 = load i32, i32* %brachiosaurus, align 4
  %10 = load i32, i32* %velociraptor, align 4
  %11 = add nsw i32 %9, %10
  ret i32 %11
}

; Function Attrs: nounwind uwtable
define i32 @anticoelho(i32 %coelho) #0 {
  %1 = alloca i32, align 4
  %basco = alloca i32, align 4
  store i32 %coelho, i32* %1, align 4
  %2 = load i32, i32* %1, align 4
  %3 = sub nsw i32 0, %2
  %4 = load i32, i32* %basco, align 4
  %5 = mul nsw i32 %3, %4
  store i32 %5, i32* %basco, align 4
  %6 = load i32, i32* %basco, align 4
  ret i32 %6
}

; Function Attrs: nounwind uwtable
define i32 @hmm() #0 {
  ret i32 -1
}

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
  %1 = alloca i32, align 4
  store i32 0, i32* %1, align 4
  %2 = call i32 @hmm()
  %3 = call i32 @arroz(i32 3, i32 4)
  ret i32 %3
}

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.8.0-2ubuntu4 (tags/RELEASE_380/final)"}
