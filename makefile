SHELL = /bin/bash
PARSER = cymbol
DBPARSER = cymboldb
DBDIR = debug/
CC = clang

$(PARSER): lex.yy.c y.tab.c y.tab.h ast.c ast.h backend.c
	$(CC) lex.yy.c y.tab.c ast.c backend.c -O3 -o $@

y.tab.c y.tab.h: $(PARSER).y
	yacc -v -d  $(PARSER).y

lex.yy.c: $(PARSER).l
	lex $(PARSER).l

# debug version
debug: $(DBPARSER)

$(DBPARSER): $(addprefix $(DBDIR), lex.yy.c y.tab.c y.tab.h)
	$(CC) $(addprefix $(DBDIR), lex.yy.c y.tab.c) -O3 -o $@

$(addprefix $(DBDIR), y.tab.c y.tab.h): $(addprefix $(DBDIR), $(PARSER).y)
	cd $(DBDIR); yacc -v -d $(PARSER).y

$(addprefix $(DBDIR), lex.yy.c): $(addprefix $(DBDIR), $(PARSER).l)
	cd $(DBDIR); lex $(PARSER).l

# etc
clean:
	-rm -f $(shell echo {$(DBDIR),}{lex.yy.c,y.tab.c,y.tab.h,y.output}) $(PARSER) $(DBPARSER)

