#ifndef AST_HEADER
#define AST_HEADER

#include <inttypes.h>
#include <stdbool.h>

typedef struct List_ List;
typedef union AST_ AST;

struct List_ {
    AST *ast;
    List *next;
};

union AST_ {

    union {
        char decl_type;

        struct {
            char decl_type;
            int type;
            AST *id;
            AST *paramTypeList;
            AST *block;
        } funcDecl; // 'f'

        struct {
            uintptr_t buffering;
            int type;
            AST *id;
            AST *expr;
        } varDecl; // 'v'

    } decl;


    struct {
        int num_items;
        List *first;
        List *last;
    } list; // exprList paramTypeList statBlock/block file

    struct {
        int type;
        AST *id;
    } paramType;


    struct {
        char stat_type;

        union {
            struct {
                int type;
                AST *id;
                AST *expr;
            } varDecl; // 'd'

            struct {
                AST *id;
                AST *expr;
            } assignStat; // '='

            struct {
                AST *expr;
            } returnStat; // 'r'

            struct {
                AST *expr;
            } exprStat; // 'e'
        };

    } stat; // base type


    struct {
        char expr_type;

        union {
            struct {
                AST *leftExpr;
                AST *rightExpr;
            } binExpr; // '+', '-', '*', '/'

            struct {
                AST *expr;
            } uminus; // 'm'

            struct {
                int value;
            } number; // 'n'

            struct {
                AST *id;
            } id; // 'i'

            struct {
                AST *id;
                AST *exprList;
            } funcCall; // 'c'
        };

    } expr; // base type

    struct {
        char *id_str;
        int id_hash;
        int id_length;
        // only filled at backend time
        int last_scope; // function that used it
        union {
            int ir_number;
            int cte_value;
        };
        bool is_global;
        bool is_global_in_this_scope;
        bool is_cte;
    } id;

};

extern AST *root;

void init_memory ();
AST *new_number (int num);
AST *new_id (char *id);
AST *new_idExpr (AST *id);
AST *new_binExpr (AST *leftExpr, char op, AST *rightExpr);
AST *new_uminus (AST *expr);
AST *new_funcCall (AST *id, AST *exprList);
AST *new_list (AST *expr);
AST *add_to_list (AST *exprList, AST *expr);
AST *new_exprStat (AST *expr);
AST *new_returnStat (AST *expr);
AST *new_assignStat (AST *id, AST *expr);
AST *new_varDecl (int type, AST *id, AST *expr);
AST *new_paramType (int type, AST *id);
AST *new_funcDecl (int type, AST *id, AST *paramTypeList, AST *block);
AST *new_file (AST *decl);
AST *add_to_file (AST *file, AST *decl);

#endif // AST_HEADER
