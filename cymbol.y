%{
/* C declarations used in actions */
#   include <stdio.h>
#   include <stdlib.h>
#   include <string.h>
#   include <inttypes.h>
#   include "ast.h"
#   define print(...) fprintf(stderr, __VA_ARGS__)

    extern int yylex (void);
    void yyerror (char *str);
    AST *meet_file (AST *root);
%}
/* yacc definitions */
%union { int num; char *id; AST *ast; } /* types that may be returned */
%start file              /* start token */
%token <num> TYPEINT RETURN
%token <num> NUMBER       /* type of terminals (on the right side of the grammar) */
%token <id> ID
%type <num> type
%type <ast> id expr funcCall exprList exprStat stat returnStat assignStat varDecl statBlock block paramType paramTypeList funcDecl file
/*%type <num> file expr term assignment call*/ /* type of non-terminals on the left side of the grammar */
/*%type <id> definition param */
%left '+' '-'             /* orders of precedence */
%left '*' '/'
%left UMINUS UPLUS        /* unary minus */

%%

/* description of expected inputs :  corresponding actions (in C) */
file       : funcDecl           { $$ = new_file($1); }
           | file funcDecl      { $$ = add_to_file($1, $2); }
           | varDecl            { $$ = new_file($1); }
           | file varDecl       { $$ = add_to_file($1, $2); }
           ;

varDecl    : type id '=' expr ';'    { $$ = new_varDecl($1, $2, $4); }
           | type id ';'             { $$ = new_varDecl($1, $2, NULL); }
           ;

type       : TYPEINT            { $$ = $1; }
           ;

funcDecl   : type id '(' paramTypeList ')' block   { $$ = new_funcDecl($1, $2, $4, $6); }
           | type id '(' ')' block                 { $$ = new_funcDecl($1, $2, NULL, $5); }
           ;

paramTypeList  : paramType                       { $$ = new_list($1); }
               | paramTypeList ',' paramType     { $$ = add_to_list($1, $3); }
               ;

paramType  : type id            { $$ = new_paramType($1, $2); }
           ;

block      : '{' statBlock '}'  { $$ = $2; }
           | '{' '}'            { $$ = NULL; }
           ;

assignStat : id '=' expr ';'    { $$ = new_assignStat($1, $3); }
           ;

returnStat : RETURN expr ';'    { $$ = new_returnStat($2); }
           ;

exprStat   : expr ';'           { $$ = new_exprStat($1); }
           ;

exprList   : expr               { $$ = new_list($1); }
           | exprList ',' expr  { $$ = add_to_list($1, $3); }
           ;

statBlock  :  stat               { $$ = new_list($1); }
           |  statBlock stat     { $$ = add_to_list($1, $2); }
           ;

stat       : varDecl            { $$ = $1; }
           | returnStat         { $$ = $1; }
           | assignStat         { $$ = $1; }
           | exprStat           { $$ = $1; }
           ;

funcCall   : id '(' exprList ')'   { $$ = new_funcCall($1, $3); }
           | id '(' ')'            { $$ = new_funcCall($1, NULL); }
           ;

expr       : expr '+' expr          { $$ = new_binExpr($1, '+', $3); }
           | expr '-' expr          { $$ = new_binExpr($1, '-', $3); }
           | expr '*' expr          { $$ = new_binExpr($1, '*', $3); }
           | expr '/' expr          { $$ = new_binExpr($1, '/', $3); }
           | '-' expr %prec UMINUS  { $$ = new_uminus($2); }
           | '+' expr %prec UPLUS   { $$ = $2; }
           | '(' expr ')'           { $$ = $2; }
           | funcCall               { $$ = $1; }
           | NUMBER                 { $$ = new_number(yyval.num); }
           | id                     { $$ = new_idExpr($1); }
           ;

id         : ID                     { $$ = new_id(yyval.id); }
           ;

%%

/* C code */
int main (void) {
    printf("; size of AST = %lu\n", sizeof(AST));
    init_memory();
    int retval =  yyparse();
    printf("; root = %p\n", root);
    meet_file(root);
    return retval;
}

void yyerror (char *str) {
    fprintf(stderr, "%s\n", str);
}

