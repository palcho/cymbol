#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "ast.h"

#define print(...) fprintf(stderr, __VA_ARGS__)
#define INI_MAX_COLUMNS 4
#define INI_MAX_ROWS 256

AST *root = NULL;

typedef struct Memory_ Memory;

struct Memory_ {
    union {
        AST **tree;
        List **ptrs;
        char **strings;
        AST **array;
    };
    int current_column;
    int current_row; // at current column
    int columns_allocated;
    int rows_allocated; // at current column

} ast, astptr, str, id_buffer;


void init_memory () {
    ast.current_column = astptr.current_column = str.current_column = id_buffer.current_column = 0;
    ast.current_row = astptr.current_row = str.current_row = id_buffer.current_row = 0;
    ast.columns_allocated = astptr.columns_allocated = str.columns_allocated = id_buffer.columns_allocated = INI_MAX_COLUMNS;
    ast.rows_allocated = astptr.rows_allocated = str.rows_allocated = id_buffer.rows_allocated = INI_MAX_ROWS;

    ast.tree = malloc(INI_MAX_COLUMNS * sizeof(AST *));
    astptr.ptrs = malloc(INI_MAX_COLUMNS * sizeof(List *));
    str.strings = malloc(INI_MAX_COLUMNS * sizeof(char *));
    id_buffer.array = malloc(INI_MAX_COLUMNS * sizeof(AST *));

    assert(ast.tree && astptr.ptrs && str.strings && id_buffer.array);

    ast.tree[0] = malloc(INI_MAX_ROWS * sizeof(AST));
    astptr.ptrs[0] = malloc(INI_MAX_ROWS * sizeof(List));
    str.strings[0] = malloc(INI_MAX_ROWS * sizeof(char));
    id_buffer.array[0] = malloc(INI_MAX_ROWS * sizeof(AST));

    assert(ast.tree[0] && astptr.ptrs[0] && str.strings[0] && id_buffer.array[0]);
}

#define row_full(x) ((x)->current_row + input_length > (x)->rows_allocated)
#define all_columns_full(x) ((x)->current_column + 1 >= (x)->columns_allocated)

void resize_if_needed (Memory* mem, unsigned long type_size, int input_length) {
    if (row_full(mem)) {
        if (all_columns_full(mem)) {
            mem->columns_allocated *= 2;
            mem->ptrs = realloc(mem->ptrs, mem->columns_allocated * sizeof(void *));
            assert(mem->ptrs);
        }
        mem->current_column++;
        mem->rows_allocated *= 2;
        mem->ptrs[mem->current_column] = malloc(mem->rows_allocated * type_size);
        assert(mem->ptrs[mem->current_column]);
        mem->current_row = 0;
    }
}

AST *alloc_ast () {
    //print("ast: %d %d %d %d\n", ast.columns_allocated, ast.rows_allocated, ast.current_column, ast.current_row);
    resize_if_needed(&ast, sizeof(AST), 1);
    int previous_row = ast.current_row++;
    return &ast.tree[ast.current_column][previous_row];
}

char *alloc_str (int length) {
    //print("str: %d %d %d %d\n", str.columns_allocated, str.rows_allocated, str.current_column, str.current_row);
    resize_if_needed(&str, sizeof(char), length);
    int previous_row = str.current_row;
    str.current_row += length;
    return &str.strings[str.current_column][previous_row];
}

// a mess
List *alloc_astptr () {
    resize_if_needed(&astptr, sizeof(List), 1);
    int previous_row = astptr.current_row++;
    return &astptr.ptrs[astptr.current_column][previous_row];
}

AST *alloc_id () {
    resize_if_needed(&id_buffer, sizeof(AST), 1);
    int previous_row = id_buffer.current_row++;
    return &id_buffer.array[id_buffer.current_column][previous_row];
}


AST *new_number (int num) {
    //print("NEW_NUMBER\n");
    AST *ast = alloc_ast();
    ast->expr.expr_type = 'n';
    ast->expr.number.value = num;
    return ast;
}

int jenkins_hash (char *string) {
    int hash = 0;

    do {
        hash += *string;
        hash += hash << 10;
        hash ^= hash >> 6;
    } while (*string++);

    hash += hash << 3;
    hash ^= hash >> 11;
    hash += hash << 15;
    return hash;
}

AST *locate_id (char *string, int hash, int length) {
    AST *ptr; 
    int i, j;
    //print("hash: 0x%08x, length: %d\n", hash, length);
    for (i = 0; i < id_buffer.current_column; i++) {
        for (j = 0, ptr = id_buffer.array[i]; j < INI_MAX_ROWS << i; j++, ptr++) {
            if (ptr->id.id_hash == hash && ptr->id.id_length == length && strcmp(ptr->id.id_str, string) == 0) return ptr;
        }
    }
    for (j = 0, ptr = id_buffer.array[id_buffer.current_column]; j < id_buffer.current_row; j++, ptr++) {
        if (ptr->id.id_hash == hash && ptr->id.id_length == length) return ptr;
    }
    return NULL;
}


AST *new_id (char *id) {
    //print("NEW_ID\n");
    int length = strlen(id) + 1;
    int hash = jenkins_hash(id);
    AST *ast = locate_id(id, hash, length);
    if (ast == NULL) {
        //print("id %s is brand new\n", id);
        ast = alloc_id();
        ast->id.id_length = length;
        ast->id.id_hash = hash;
        ast->id.id_str = strcpy(alloc_str(ast->id.id_length), id);
        ast->id.is_global = false; // default
        ast->id.last_scope = -1;
        ast->id.is_cte = true;
    }
    return ast;
}

AST *new_idExpr (AST *id) {
    AST *ast = alloc_ast();
    ast->expr.expr_type = 'i';
    ast->expr.id.id = id;
    return ast;
}

AST *new_binExpr (AST *leftExpr, char op, AST *rightExpr) {
    switch (op) {
        case '+':
            if (leftExpr->expr.expr_type == 'n' && leftExpr->expr.number.value == 0)
                return rightExpr;
            if (rightExpr->expr.expr_type == 'n' && rightExpr->expr.number.value == 0)
                return leftExpr;
            break;
        case '-':
            if (rightExpr->expr.expr_type == 'n' && rightExpr->expr.number.value == 0)
                return leftExpr;
            break;
        case '*':
            if (leftExpr->expr.expr_type == 'n') {
                if (leftExpr->expr.number.value == 0) return leftExpr;
                if (leftExpr->expr.number.value == 1) return rightExpr;
            }
            if (rightExpr->expr.expr_type == 'n') {
                if (rightExpr->expr.number.value == 0) return rightExpr;
                if (rightExpr->expr.number.value == 1) return leftExpr;
            }
            break;
        case '/':
            if (leftExpr->expr.expr_type == 'n' && leftExpr->expr.number.value == 0)
                return leftExpr;
            if (rightExpr->expr.expr_type == 'n' && rightExpr->expr.number.value == 1)
                return leftExpr;
            break;
        default: break;
    }
    //print("NEW_BIN_EXPR\n");
    AST *ast = alloc_ast();
    ast->expr.expr_type = op;
    ast->expr.binExpr.leftExpr = leftExpr;
    ast->expr.binExpr.rightExpr = rightExpr;
    return ast;
}

AST *new_uminus (AST *expr) {
    if (expr->expr.expr_type == 'n' && expr->expr.number.value == 0)
        return expr;
    //print("NEW_UNARY_EXPR\n");
    AST *ast = alloc_ast();
    ast->expr.expr_type = 'm';
    ast->expr.uminus.expr = expr;
    return ast;
}

AST *new_funcCall (AST *id, AST *exprList) {
    //print("NEW_FUNCTION_CALL\n");
    AST *ast = alloc_ast();
    ast->expr.expr_type = 'c';
    ast->expr.funcCall.id = id;
    ast->expr.funcCall.exprList = exprList;
    return ast;
}

AST *new_list (AST *node) {
    //print("NEW_EXPR_LIST\n");
    AST *ast = alloc_ast();
    ast->list.num_items = 1;
    ast->list.first = alloc_astptr();
    *ast->list.first = (List){ node, NULL };
    ast->list.last = ast->list.first;
    return ast;
}

AST *add_to_list (AST *list, AST *node) {
    //print("ADD_EXPR_TO_LIST\n");
    list->list.num_items++;
    List *tmp = alloc_astptr();
    list->list.last->next = tmp;
    *tmp = (List){ node, NULL };
    list->list.last = tmp;
    return list;
}

AST *new_file (AST *decl) {
    if (decl->decl.decl_type == 'v') {
        AST *id = decl->decl.varDecl.id;
        id->id.is_global = true;
        id->id.is_global_in_this_scope = true;
        id->id.is_cte = false;
    }
    root = new_list(decl);
    return root;
}

AST *add_to_file (AST *file, AST *decl) {
    if (decl->decl.decl_type == 'v') {
        AST *id = decl->decl.varDecl.id;
        id->id.is_global = true;
        id->id.is_global_in_this_scope = true;
    }
    return add_to_list(file, decl);
}


AST *new_exprStat (AST *expr) {
    //print("NEW_EXPR_STAT\n");
    AST *ast = alloc_ast();
    ast->stat.stat_type = 'e';
    ast->stat.exprStat.expr = expr;
    return ast;
}

AST *new_returnStat (AST *expr) {
    //print("NEW_RETURN_STAT\n");
    AST *ast = alloc_ast();
    ast->stat.stat_type = 'r';
    ast->stat.returnStat.expr = expr;
    return ast;
}

AST *new_assignStat (AST *id, AST *expr) {
    //print("NEW_ASSIGN_STAT\n");
    AST *ast = alloc_ast();
    ast->stat.stat_type = '=';
    ast->stat.assignStat.id = id;
    ast->stat.assignStat.expr = expr;
    return ast;
}

AST *new_varDecl (int type, AST *id, AST *expr) {
    //print("NEW_VAR_DECL\n");
    AST *ast = alloc_ast();
    ast->decl.decl_type = 'd';
    ast->decl.varDecl.type = type;
    ast->decl.varDecl.id = id;
    ast->decl.varDecl.expr = expr;
    return ast;
}

AST *new_paramType (int type, AST *id) {
    //print("NEW_PARAM_TYPE\n");
    AST *ast = alloc_ast();
    ast->paramType.type = type;
    ast->paramType.id = id;
    return ast;
}

AST *new_funcDecl (int type, AST *id, AST *paramTypeList, AST *block) {
    //print("NEW_FUNC_DECL\n");
    AST *ast = alloc_ast();
    ast->decl.decl_type = 'f';
    ast->decl.funcDecl.type = type;
    ast->decl.funcDecl.id = id;
    ast->decl.funcDecl.paramTypeList = paramTypeList;
    ast->decl.funcDecl.block = block;
    return ast;
}

